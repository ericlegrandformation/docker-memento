# MEMENTO DOCKER

  

## Liste des commandes usuelles

|Commande |Effet |
|-------------------------------|-----------------------------|
| `docker ps` | lister les conteneurs en train de tourner |
| `docker ps -a` | lister les conteneurs arrêtés et tournants |
| `docker inspect <num_ou_nom_conteneur>` | donne des informations concernant le conteneur |
| `docker search <nom_conteneur>` | recherche une image docker dans le dépôt par défaut |
| `docker pull <image>[:<version>]` |récupére une image (depuis un registry docker hub par défaut) sans la lancer |
| `docker run <image> [<command>]` |récupére, lancer un conteneur à partir d'une image et lancer une commande dans le conteneur |
| `docker run --rm --name <nom_conteneur> <image>` | lance une image avec un nom et en mode suppression automatique (lorsqu'on arrête le conteneur ) |
| `docker run -it <nom_conteneur> bash` | démarre un conteneur de l'image <nom_image> en mode interactif avec bash lancé au démarrage du conteneur |
| `docker exec -it <num_ou_nom_conteneur> bash` | se connecte au conteneur <num_ou_nom_conteneur> en lancant bash |
| `docker start <num_ou_nom_conteneur>` | démarre un conteneur |
| `docker stop <num_ou_nom_conteneur>` | arrête un conteneur |
| `docker kill <num_ou_nom_conteneur>` | arrête de force un conteneur récalcitrant |
| `docker rm <num_ou_nom_conteneur>` | supprime un conteneur arrêté (`--force` pour supprimer un conteneur allumé) |
| `docker image ls` | liste les images |
| `docker image rm <id_ou_nom_image>` | Supprime une image |

## Options utiles
|Commande |Option |
|-------------------------------|-----------------------------|
| `docker <commande> --help` | help permet d'afficher l'aide pour la commande <commande> |




## Liste de commandes radicales

|Commande |Effet |
|-------------------------------|-----------------------------|
| `docker container stop $(docker container ls -aq)` | Stop tous les containers en train de tourner |
| `docker container rm $(docker ps -a -q)` | Supprime tous les containers en train de tourner |
| `docker rm $(docker ps -a -q)` | Supprime tous les containers en train de tourner |
| `docker rmi $(docker ps -a -q)` | Supprime tous les images |
| `docker container prune -a ` | Nettoie toutes les containers |
| `docker image prune -a ` | Nettoie toutes les images | 
| `docker volume prune ` |  Nettoie tous les volumes |
| `docker network prune ` |  Nettoie tous les réseaux |
| `docker system prune` | Nettoie tout le système des containers aux réseaux inutilisés |


## Versions

  

**Dernière version stable :** 0.1

  

**Dernière version :** 0.1

  

Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/ansible.git)

  

## Auteurs

  

*  **Eric Legrand**  _alias_  [@ericlegrand](https://framagit.org/ericlegrandformation/ansible.git)

  

avec le soutien de Messieurs Hugh Norris et Elie Gavoty.

  

```